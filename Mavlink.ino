#include "NewPing.h"
#include "C:\Users\LG\Desktop\siva\prx\Mavlink\lib\mavlink\common\mavlink.h"

//==================================INITIALIZATIONS======================================//
/*Initialization of the sensor pins. SONOR object using "NewPing"
   NewPing <NAME> (Trigger, Echo, MAXDIST);
   The value of MAXDIST is the maximum distance the library measures.
   If any Echo returns a value greater than said distance, it is automatically discarded*/
NewPing sonar0(3, 4, 300);
NewPing sonar1(5, 6, 300);
NewPing sonar2(7, 8, 300);
NewPing sonar3(9, 10, 300);
NewPing sonar4(11, 12, 100);
 
//Variable used to control the heartbeat sent every second
unsigned long HeartbeatTime = 0; 

/*Variables used to send only one RC Override each time
it is modified, and do not overload the redundant order controller*/
uint16_t Pitch = 0;
uint16_t Roll  = 0;
uint16_t PitchOut = 0;
uint16_t RollOut  = 0;
uint16_t PitchOutTemp = 0;
uint16_t RollOutTemp  = 0;
uint8_t n         = 0;

#define NDistances     5
#define CloseDist  100  //Distance to which the control begins to act
#define AltMin          70 //Height at which the control begins to act
#define DistMin         50 //Minimum difference between two distances of the same axis to move.
#define Compensation    800 //Compensation time in ms.

//Sensor data set
struct Sensors {
  uint16_t Distances[NDistances]  = {0};
  uint16_t MeasuredDistance          = 0;
  bool Search                        = false;
  bool Active                       = false;
  unsigned long TimeComposition       = 0;
};

//Sensor variable initialization
#define NSensors 5
Sensors Sensor[NSensors];

//====================================PROGRAM============================================//

void setup() {
  Serial.begin(57600);
  
}

void loop() {
  if ( (millis() - HeartbeatTime) > 1000 ) {
    HeartbeatTime = millis();
    FHeartBeat();
  }
  FSensors();
  FRCOverride();
}

//===========================================FUNCTIONS====================================//
//Measuring Sensors
void FSensors() {
  ShiftArrays();
  MeasureSensors();
  MeasuredDistance();
  CheckDistance();
}

//Sending the motion commands according to the Distances detected by the Sensors
void FRCOverride() {
  mavlink_message_t msg;
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  uint16_t len;
  
  Pitch  = CheckPitch(Pitch);
  Roll   = CheckRoll(Roll);
  
  InertiaCompensation();

  if( Pitch != PitchOutTemp || Roll != RollOutTemp ){
    n = 0;
    PitchOutTemp = Pitch;
    RollOutTemp  = Roll;
  }else{
    n += 1;
    if(n == 4){
      RollOut = RollOutTemp;
      PitchOut = PitchOutTemp;
      RCOverride(&msg, len, buf, PitchOut, RollOut);
    }
  }  
}

//Moves each array of Distances in one position
void ShiftArrays() {
  for (uint8_t i = 0; i < NSensors; i++) {
    for (uint8_t j = NDistances - 1; j > 0; j--) {
      Sensor[i].Distances[j] = Sensor[i].Distances[j - 1];
    }
  }
}

//==================================Sensors=====================================//
//Sensors are measured, and placed at position 0 of each array
void MeasureSensors() {
  Sensor[0].Distances[0] = sonar0.ping_cm();
  Sensor[1].Distances[0] = sonar1.ping_cm();
  Sensor[2].Distances[0] = sonar2.ping_cm();
  Sensor[3].Distances[0] = sonar3.ping_cm();
  Sensor[4].Distances[0] = sonar4.ping_cm();
}

//The average of all the Distances is realized. The 0 are discarded
void MeasuredDistance() {
  for (uint8_t i = 0; i < NSensors; i++) {
    int Total   = 0;
    uint8_t Num = 0;
    for (uint8_t j = 0; j < NDistances; j++) {
      if (Sensor[i].Distances[j] != 0  && Sensor[i].Distances[j] < 300) {
        Total += Sensor[i].Distances[j];
        Num += 1;
      }
    }
    if (Num > 3) {
      Sensor[i].MeasuredDistance = Total / Num;
    } else {
      Sensor[i].MeasuredDistance = 0;
    }
  }
  #ifndef true
  #define static uint16_t abc == 1
  #define abc = abc + 1
  #endif
  
  
  /*Serial.print("\n\rDistances: ");
  Serial.print(Sensor[0].MeasuredDistance);
  Serial.print(",");
  Serial.print(Sensor[1].MeasuredDistance);
  Serial.print(",");
  Serial.print(Sensor[2].MeasuredDistance);
  Serial.print(",");
  Serial.print(Sensor[3].MeasuredDistance);
  Serial.print(",");
  Serial.print(Sensor[4].MeasuredDistance);
  Serial.print("cm\n\r");
  #endif*/
  
  
  
}

//Checking the threshold value
void CheckDistance() {
  
  for (uint8_t i = 0; i < NSensors; i++) {
    if (Sensor[i].MeasuredDistance != 0 && Sensor[i].MeasuredDistance < CloseDist) {
      Sensor[i].Search = true;
    } else {
      Sensor[i].Search = false;
    }
  }
}

//========================MOVEMENT=========================//
uint16_t CheckPitch(uint16_t Pitch) {
  int16_t Difference = Sensor[0].MeasuredDistance - Sensor[2].MeasuredDistance;
  if( Sensor[4].MeasuredDistance > AltMin || Sensor[4].MeasuredDistance == 0 ) {
    if( abs(Difference) > DistMin ) {
    //Difference greater than 30 between both Sensors
    if( Sensor[0].Search == true ) {
      //Detects the front
      if( Sensor[2].Search == true ) {
        //Detects the rear
        if( Sensor[0].MeasuredDistance < Sensor[2].MeasuredDistance ) {
          //The front sensor has a shorter distance
          return( Pitch = RCValue( Sensor[0].MeasuredDistance, 1 ) );
        }else{
          //The rear sensor has a smaller distance
          return( Pitch = RCValue( Sensor[2].MeasuredDistance, 0 ) );
        }
      }else{
        //Detects the front but not the rear
        return( Pitch = RCValue( Sensor[0].MeasuredDistance, 1 ) );
      }
    }else {
      //Does not detect the front
      if( Sensor[2].Search == true ) {
        //Detects the rear
        return( Pitch = RCValue( Sensor[2].MeasuredDistance, 0 ) );
      }else{
        //Both have a distance greater than 150
        return( Pitch = 0 );
      }
    }
  }else if( Sensor[0].Search == true && Sensor[2].MeasuredDistance == 0 ) {
    //It detects the one from the front, and the one from behind when it does not detect anything, returns 0
    return( Pitch = RCValue( Sensor[0].MeasuredDistance, 1 ) );
    }else if ( Sensor[0].MeasuredDistance == 0 && Sensor[2].Search == true ) {
      //Same but the opposite
      return( Pitch = RCValue( Sensor[2].MeasuredDistance, 0 ) );
      }else {
        //It does not detect any. Both at 0
        return( Pitch = 0 );
      }
  }else{
    return( Pitch = 0 );
  }
}

uint16_t CheckRoll(uint16_t Roll) {  
  int16_t Difference = Sensor[1].MeasuredDistance - Sensor[3].MeasuredDistance;
  if( Sensor[4].MeasuredDistance > AltMin || Sensor[4].MeasuredDistance == 0 ) {
    if( abs(Difference) > DistMin ) {
      //Difference greater than 20 between Distances
      if( Sensor[1].Search == true ) {
        //Detects the right
        if( Sensor[3].Search == true ) {
          //Detects the left
          if( Sensor[1].MeasuredDistance < Sensor[3].MeasuredDistance ) {
            //The right sensor has a smaller distance
            return( Roll = RCValue( Sensor[1].MeasuredDistance, 0 ) );
          }else{
            //The left sensor has a smaller distance
            return( Roll = RCValue( Sensor[3].MeasuredDistance, 1 ) );
          }
        }else{
          //Detects the right, but not the left
          return( Roll = RCValue( Sensor[1].MeasuredDistance, 0 ) );
        }
      }else {
        //Does not detect the right
        if( Sensor[3].Search == true ) {
          //Detects the left
          return( Roll = RCValue( Sensor[3].MeasuredDistance, 1 ) );
        }else{
          //Both have a distance greater than 150
          return( Roll = 0 );
        }
      }
    }else if( Sensor[1].Search == true && Sensor[3].MeasuredDistance == 0 ) {
      //It detects the right, and the left when it does not detect anything, returns 0
      return( Roll = RCValue( Sensor[1].MeasuredDistance, 0 ) );
      }else if ( Sensor[1].MeasuredDistance == 0 && Sensor[3].Search == true ) {
        //Same but the opposite
        return( Roll = RCValue( Sensor[3].MeasuredDistance, 1 ) );
        }else {
          //It does not detect any. Both at 0
          return( Roll = 0 );
        }
  }else {
    return( Roll = 0 );
  }
}

//Returns an output value depending on the distance
//The greater the distance, the less the need for movement. 
//The variable "Increase" is to know in which direction it is.
uint16_t RCValue( uint16_t Dist, bool Inc ) {
  if( Dist < 30 ) {
    if( Inc == true ) {
      return( 1700 );
    }else{
      return( 1300 );
    }
  }else if( Dist < 90 ) {
    if( Inc == true ) {
      return( 1675 );
    }else{
      return( 1325 );
    }
  }else if( Dist < 150 ) {
    if( Inc == true ) {
      return( 1650 );
    }else{
      return( 1350 );
    }
  }
}

void InertiaCompensation(){

  if(PitchOut > 1500 && Sensor[0].Active == false && Sensor[2].Active == false){
    Sensor[0].Active = true;
  }else if(PitchOut < 1500 && PitchOut != 0 && Sensor[2].Active == false && Sensor[0].Active == false){
    Sensor[2].Active = true;
  }else if(PitchOut == 0 && Sensor[0].Active == true && Sensor[0].TimeComposition == 0){
    Sensor[0].TimeComposition = millis();
  }else if(PitchOut == 0 && Sensor[2].Active == true && Sensor[2].TimeComposition == 0){
    Sensor[2].TimeComposition = millis();
  }

  if(RollOut > 1500 && Sensor[3].Active == false && Sensor[1].Active == false){
    Sensor[3].Active = true;
  }else if(RollOut < 1500 && RollOut != 0 && Sensor[1].Active == false && Sensor[3].Active == false){
    Sensor[1].Active = true;
  }else if(RollOut == 0 && Sensor[1].Active == true && Sensor[1].TimeComposition == 0){
    Sensor[1].TimeComposition = millis();
  }else if(RollOut == 0 && Sensor[3].Active == true && Sensor[3].TimeComposition == 0){
    Sensor[3].TimeComposition = millis();
  }

  for(int i = 0; i < 4; i++){
    if(Sensor[i].TimeComposition != 0 && (Sensor[i].TimeComposition + Compensation > millis())){
      switch(i){
        case 0:
          Pitch = 1300;
          break;
        case 1:
          Roll = 1700;
          break;
        case 2:
          Pitch = 1700;
          break;
        case 3:
          Roll = 1300;
          break;
        default:
          break;
      }
    }else if(Sensor[i].TimeComposition != 0){
      switch(i){
        case 0:
        case 2:
          PitchOut = 0;
          Sensor[i].Active = false;
          Sensor[i].TimeComposition = 0;
          break;
        case 1:
        case 3:
          RollOut = 0;
          Sensor[i].Active = false;
          Sensor[i].TimeComposition = 0;
          break;
        default:
          break;
      }
    }
  }
  
}

//============================MAVLINK==========================//
//Sending HeartBeat every second
void FHeartBeat() {
  mavlink_message_t msg;
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  uint16_t len;
  // System ID = 255 = GCS
  mavlink_msg_heartbeat_pack(255, 0, &msg, MAV_TYPE_QUADROTOR, MAV_AUTOPILOT_GENERIC, 0, 1, 0);

  // Copy the message to send buffer
  len = mavlink_msg_to_send_buffer(buf, &msg);

  // Send the message (.write sends as bytes)
  Serial.write(buf, len);

  //Serial.write("\n\rHeartBeat\n\r");
}

void RCOverride(mavlink_message_t *msg, uint16_t len, uint8_t *buf, uint16_t PitchOut, uint16_t RollOut) {
  //Package and send calculated Pitch and Roll data. Only send if the data is new
  mavlink_msg_rc_channels_override_pack(255, 0 , msg, 1, 0, RollOut, PitchOut, 0, 0, 0, 0, 0, 0);
  len = mavlink_msg_to_send_buffer(buf, msg);
  Serial.write(buf, len);
  Serial.print("\n\rPitch: ");
  Serial.print(PitchOut);
  Serial.print(",");
  Serial.print(" Roll: ");
  Serial.print(RollOut);
}

  /*//Armar Dron
    //Pack the message
    //uint16_t mavlink_msg_command_long_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,uint8_t target_system, uint8_t target_component, uint16_t command, uint8_t confirmation, float param1, float param2, float param3, float param4, float param5, float param6, float param7)
    mavlink_msg_command_long_pack(255, 0, &msg, 1, 0, MAV_CMD_COMPONENT_ARM_DISARM, 0, 1, 0, 0, 0, 0, 0, 0);

    len = mavlink_msg_to_send_buffer(buf, &msg);

    // Send the message (.write sends as bytes)
    Serial.write(buf, len);
    delay(1000);*/

        /*mavlink_msg_rc_channels_override_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
    uint8_t target_system, uint8_t target_component, uint16_t chan1_raw, uint16_t chan2_raw, uint16_t chan3_raw,
    uint16_t chan4_raw, uint16_t chan5_raw, uint16_t chan6_raw, uint16_t chan7_raw, uint16_t chan8_raw)*/
    
  /*Channel 1 = Roll
    Channel 2 = Pitch
    Channel 3 = Throttle
    Channel 4 = Yaw*/

  /*Sensor0 = Delantero
    Sensor1 = Derecha
    Sensor2 = Trasero
    Sensor3 = Izquierda*/
