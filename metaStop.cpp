#include "NewPing.h"
#include "C:\Users\LG\Desktop\siva\prx\Mavlink\lib\mavlink\common\mavlink.h"

//==================================INITIALIZATIONS======================================//
/*Initialization of the sensor pins. SONOR object using "NewPing"
   NewPing <NAME> (Trigger, Echo, MAXDIST);
   The value of MAXDIST is the maximum distance the library measures.
   If any Echo returns a value greater than said distance, it is automatically discarded*/
NewPing sonar0(3, 4, 300);
NewPing sonar1(5, 6, 300);
NewPing sonar2(7, 8, 300);
NewPing sonar3(9, 10, 300);
//NewPing sonar4(11, 12, 100);
 
//Variable used to control the heartbeat sent every second
unsigned long HeartbeatTime = 0;

//Sensor data set
struct Sensors {
  uint16_t Distances[NDistances]  = {0};
  uint16_t MeasuredDistance          = 0;
  bool Search                        = false;
  bool Active                       = false;
  unsigned long TimeComposition       = 0;
};

//Sensor variable initialization
#define NSensors 4
Sensors Sensor[NSensors];

//====================================PROGRAM============================================//

void setup() {
  Serial.begin(57600);
  
}

void loop() {
  if ( (millis() - HeartbeatTime) > 1000 ) {
    HeartbeatTime = millis();
    FHeartBeat();
  }
  FSensors();
  FRCOverride();
}

//Measuring Sensors
void FSensors() {
  ShiftArrays();
  MeasureSensors();
  MeasuredDistance();
  CheckDistance();
}

//Moves each array of Distances in one position
void ShiftArrays() {
  for (uint8_t i = 0; i < NSensors; i++) {
    for (uint8_t j = NDistances - 1; j > 0; j--) {
      Sensor[i].Distances[j] = Sensor[i].Distances[j - 1];
    }
  }
}

//==================================Sensors=====================================//
//Sensors are measured, and placed at position 0 of each array
void MeasureSensors() {
  Sensor[0].Distances[0] = sonar0.ping_cm();
  Sensor[1].Distances[0] = sonar1.ping_cm();
  Sensor[2].Distances[0] = sonar2.ping_cm();
  Sensor[3].Distances[0] = sonar3.ping_cm();
  Sensor[4].Distances[0] = sonar4.ping_cm();
}

//The average of all the Distances is realized. The 0 are discarded
void MeasuredDistance() {
  for (uint8_t i = 0; i < NSensors; i++) {
    int Total   = 0;
    uint8_t Num = 0;
    for (uint8_t j = 0; j < NDistances; j++) {
      if (Sensor[i].Distances[j] != 0  && Sensor[i].Distances[j] < 300) {
        Total += Sensor[i].Distances[j];
        Num += 1;
      }
    }
    if (Num > 3) {
      Sensor[i].MeasuredDistance = Total / Num;
    } else {
      Sensor[i].MeasuredDistance = 0;
    }
  }
    

  
}

//Checking the threshold value
void CheckDistance() {
  
  for (uint8_t i = 0; i < NSensors; i++) {
    if (Sensor[i].MeasuredDistance != 0 && Sensor[i].MeasuredDistance < CloseDist) {
      Sensor[i].Search = true;
    } else {
      Sensor[i].Search = false;
    }
  }
}

